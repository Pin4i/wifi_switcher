﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WifiSwitch
{
    public partial class Version : Form
    {
        public Version()
        {
            InitializeComponent();
            WhenFormStart();
        }

        void WhenFormStart()
        {
            l_log.Text = "v.1.0     31.03.2017 \n"
                + "Первая версия программы";
        }
    }
}