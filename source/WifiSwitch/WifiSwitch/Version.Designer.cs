﻿namespace WifiSwitch
{
    partial class Version
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.l_log = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // l_log
            // 
            this.l_log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.l_log.Location = new System.Drawing.Point(0, 0);
            this.l_log.Name = "l_log";
            this.l_log.Size = new System.Drawing.Size(227, 342);
            this.l_log.Text = "*";
            // 
            // Version
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(227, 342);
            this.Controls.Add(this.l_log);
            this.Menu = this.mainMenu1;
            this.Name = "Version";
            this.Text = "Version";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label l_log;
    }
}