﻿namespace WifiSwitch
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.button_restartWifi = new System.Windows.Forms.Button();
            this.label_status = new System.Windows.Forms.Label();
            this.l_wifiStatus = new System.Windows.Forms.Label();
            this.button_exit = new System.Windows.Forms.Button();
            this.l_wait = new System.Windows.Forms.Label();
            this.button_version = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_restartWifi
            // 
            this.button_restartWifi.Location = new System.Drawing.Point(33, 158);
            this.button_restartWifi.Name = "button_restartWifi";
            this.button_restartWifi.Size = new System.Drawing.Size(170, 48);
            this.button_restartWifi.TabIndex = 0;
            this.button_restartWifi.Text = "Перезапустить WiFi";
            this.button_restartWifi.Click += new System.EventHandler(this.button1_Click);
            // 
            // label_status
            // 
            this.label_status.Location = new System.Drawing.Point(3, 12);
            this.label_status.Name = "label_status";
            this.label_status.Size = new System.Drawing.Size(57, 20);
            this.label_status.Text = "Статус:";
            // 
            // l_wifiStatus
            // 
            this.l_wifiStatus.Location = new System.Drawing.Point(66, 12);
            this.l_wifiStatus.Name = "l_wifiStatus";
            this.l_wifiStatus.Size = new System.Drawing.Size(100, 20);
            this.l_wifiStatus.Text = "l_wifiStatus";
            // 
            // button_exit
            // 
            this.button_exit.Location = new System.Drawing.Point(33, 231);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(170, 49);
            this.button_exit.TabIndex = 6;
            this.button_exit.Text = "Выход";
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // l_wait
            // 
            this.l_wait.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.l_wait.Location = new System.Drawing.Point(31, 108);
            this.l_wait.Name = "l_wait";
            this.l_wait.Size = new System.Drawing.Size(174, 26);
            // 
            // button_version
            // 
            this.button_version.Location = new System.Drawing.Point(3, 308);
            this.button_version.Name = "button_version";
            this.button_version.Size = new System.Drawing.Size(40, 20);
            this.button_version.TabIndex = 9;
            this.button_version.Text = "v.1.0";
            this.button_version.Click += new System.EventHandler(this.button_version_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(227, 342);
            this.Controls.Add(this.button_version);
            this.Controls.Add(this.l_wait);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.l_wifiStatus);
            this.Controls.Add(this.label_status);
            this.Controls.Add(this.button_restartWifi);
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_restartWifi;
        private System.Windows.Forms.Label label_status;
        private System.Windows.Forms.Label l_wifiStatus;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.Label l_wait;
        private System.Windows.Forms.Button button_version;
    }
}

