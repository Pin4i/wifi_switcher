﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Threading;
using AutoUpdaterDotNET;
using System.Globalization;

namespace WifiSwitch
{
    public partial class Form1 : Form
    {
        delegate void InvokeDelegate();
        //System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;

        public Form1()
        {
            InitializeComponent();
            WhenFormStart();           
        }

      

        void WhenFormStart()
        {
            try
            {
                l_wait.Visible = false;
                l_wifiStatus.Text = Read();
                if (l_wifiStatus.Text == "WiFi выключен")                
                    l_wifiStatus.ForeColor = Color.Red;
                
                else
                    l_wifiStatus.ForeColor = Color.Green;                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }       

        void RebootWiFi()
        {
            try
            {
                Set(0);
                Set(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        string Read()
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(@"\DSIC\WirelessPower", true); //set to true if you need write access to the key.

            //Get registry key value
            int value = (int)key.GetValue("WLANPower");

            if (value == 1)
                return "WiFi включен";

            else
                return "WiFi выключен";

            //int value = (int)key3.GetValue("WLANPower"); 
            //label_status.Text += " " + valu;            
        }

        static void Set(int off_or_on)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(@"\DSIC\WirelessPower", true); //set to true if you need write access to the key.

            key.SetValue("WLANPower", off_or_on, RegistryValueKind.DWord);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Start_Settings_label();
                RebootWiFi();
                Sleep();
                Finish_Settings_label();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Start_Settings_label()
        {
            l_wait.Visible = true;
            l_wait.Text = "Подождите...";
        }
        void Sleep()
        {
            Thread.Sleep(2000);
        }
        void Finish_Settings_label()
        {
            //l_wait.Visible = false;
            l_wait.Text = "WiFi перезапущен";
            l_wifiStatus.Text = Read();

            if (l_wifiStatus.Text == "WiFi выключен")
                l_wifiStatus.ForeColor = Color.Red;

            else
                l_wifiStatus.ForeColor = Color.Green;  
        }       

        // +
        private void button_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }        

        private void button_version_Click(object sender, EventArgs e)
        {
            Version versionForm = new Version();
            versionForm.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Uncomment below line to see Russian version

            AutoUpdater.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            //If you want to open download page when user click on download button uncomment below line.

            //AutoUpdater.OpenDownloadPage = true;

            //Don't want user to select remind later time in AutoUpdater notification window then uncomment 3 lines below so default remind later time will be set to 2 days.

            //AutoUpdater.LetUserSelectRemindLater = false;
            //AutoUpdater.RemindLaterTimeSpan = RemindLaterFormat.Days;
            //AutoUpdater.RemindLaterAt = 2;

            AutoUpdater.Start("http://192.168.0.8/autoupdates/WiFiSwitcher/Version.xml");
        }
    }
}